const API_ENDPOINT: &str = "https://en.wikipedia.org/w/api.php";
const USER_AGENT: &str = "wikirace.rs/0.1 (stas.ale66@gmail.com)";
const NAMESPACE: &str = "0|14|100";

pub fn build_request(
    client: &reqwest::Client,
    prefix: &str,
    prop: &str,
    titles: &[String],
    cont: &str,
) -> reqwest::Request {
    let titles = titles.join("|");
    let namespace = format!("{prefix}namespace");
    let limit = format!("{prefix}limit");
    let mut values = vec![
        ("format", "json"),
        ("action", "query"),
        ("titles", &titles),
        ("prop", prop),
        (&namespace, NAMESPACE),
        (&limit, "max"),
    ];
    let prefix = format!("{prefix}continue");
    if !cont.is_empty() {
        values.push((&prefix, cont));
    }
    client
        .get(API_ENDPOINT)
        .query(&values)
        .header("User-Agent", USER_AGENT)
        .build()
        .unwrap()
}

pub async fn get(client: &reqwest::Client, req: reqwest::Request) -> String {
    client
        .execute(req)
        .await
        .expect("Failed to execute request")
        .text()
        .await
        .expect("Failed to get request text")
}
