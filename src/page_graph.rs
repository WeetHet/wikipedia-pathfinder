use std::collections::HashMap;
use std::sync::{Arc, RwLock};

pub struct PageGraph {
    forward: RWLockedMap,
    backward: RWLockedMap,
}

impl PageGraph {
    pub(crate) fn new() -> Self {
        Self {
            forward: RWLockedMap { inner: Default::default() },
            backward: RWLockedMap { inner: Default::default() },
        }
    }

    fn build_path(&self, midpoint: String) -> Vec<String> {
        let mut path = vec![];
        let mut cursor = self.forward.get(&midpoint).unwrap();
        while !cursor.is_empty() {
            path.push(cursor.clone());
            cursor = self.forward.get(&cursor).unwrap();
        }

        path.reverse();

        cursor = midpoint;
        while !cursor.is_empty() {
            path.push(cursor.clone());
            cursor = self.backward.get(&cursor).unwrap();
        }

        path
    }

    pub async fn search(self, from: String, to: String) -> Vec<String> {
        let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel();
        let arc_self = Arc::new(self);

        {
            let arc_self = arc_self.clone();
            let tx = tx.clone();
            tokio::spawn(async move {
                tx.send(arc_self.search_forward(from).await)
                    .expect("Failed send");
            });
        }

        {
            let arc_self = arc_self.clone();
            let tx = tx.clone();
            tokio::spawn(async move {
                tx.send(arc_self.search_backward(to).await)
                    .expect("Failed send");
            });
        }

        if let Some(midpoint) = rx.recv().await {
            arc_self.build_path(midpoint)
        } else {
            vec![]
        }
    }

    async fn search_forward(&self, start: String) -> String {
        self.forward.insert(start.clone(), "".to_string());
        let mut q = Vec::new();
        q.push(start);
        while !q.is_empty() {
            let mut pages = crate::links::links_from(std::mem::take(&mut q));
            while let Some(links) = pages.recv().await {
                for (from, destinations) in links {
                    for dest in destinations {
                        if self.check_forward(from.clone(), dest.clone(), &mut q) {
                            return dest;
                        }
                    }
                }
            }
        }
        String::new()
    }

    fn check_forward(&self, from: String, to: String, q: &mut Vec<String>) -> bool {
        if self.forward.get(&to).is_none() {
            self.forward.insert(to.clone(), from);
            q.push(to.clone());
        }

        self.backward.get(&to).is_some()
    }

    async fn search_backward(&self, start: String) -> String {
        self.backward.insert(start.clone(), "".to_string());
        let mut q = Vec::new();
        q.push(start);
        while !q.is_empty() {
            let mut pages = crate::links::links_here(std::mem::take(&mut q));
            while let Some(links) = pages.recv().await {
                for (from, destinations) in links {
                    for dest in destinations {
                        if self.check_backward(from.clone(), dest.clone(), &mut q) {
                            return dest;
                        }
                    }
                }
            }
        }
        String::new()
    }

    fn check_backward(&self, from: String, to: String, q: &mut Vec<String>) -> bool {
        if self.backward.get(&to).is_none() {
            self.backward.insert(to.clone(), from);
            q.push(to.clone());
        }

        self.forward.get(&to).is_some()
    }
}

struct RWLockedMap {
    inner: RwLock<HashMap<String, String>>,
}

impl RWLockedMap {
    fn get(&self, key: &String) -> Option<String> {
        self.inner.read().unwrap().get(key).cloned()
    }

    fn insert(&self, key: String, value: String) {
        self.inner.write().unwrap().insert(key, value);
    }
}
