use reqwest::Client;
use std::collections::HashMap;
use once_cell::sync::Lazy;
use tokio::sync::mpsc::UnboundedReceiver;

use crate::api::*;

#[derive(Debug)]
pub struct Links {
    graph: HashMap<String, Vec<String>>,
}

impl Links {
    const BORING: [&'static str; 13] = [
        "Biblioteca Nacional de España",
        "Bibliothèque nationale de France",
        "Digital object identifier",
        "Integrated Authority File",
        "International Standard Book Number",
        "International Standard Name Identifier",
        "LIBRIS",
        "Library of Congress Control Number",
        "MusicBrainz",
        "National Diet Library",
        "National Library of the Czech Republic",
        "PubMed Central",
        "Virtual International Authority File",
    ];

    pub fn add(&mut self, from: String, to: String) {
        if Self::BORING.contains(&from.as_ref())
            || Self::BORING.contains(&to.as_ref())
            || from == to
        {
            return;
        }

        self.graph.entry(from).or_default().push(to);
    }
}

impl IntoIterator for Links {
    type Item = <HashMap<String, Vec<String>> as IntoIterator>::Item;
    type IntoIter = <HashMap<String, Vec<String>> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.graph.into_iter()
    }
}

fn extract_continue(data: &serde_json::Value, subkey: &str) -> String {
    if let Some(cont) = data.get("Continue") {
        if let Some(cont_value) = cont.get(subkey) {
            return cont_value.as_str().unwrap().to_string();
        }
    }
    String::new()
}

fn extract_links(data: &serde_json::Value, subkey: &str) -> Links {
    let mut links = Links {
        graph: HashMap::new(),
    };

    let query = &data["query"];
    let pages = &query["pages"];
    for (_, page) in pages.as_object().unwrap() {
        let title = page["title"].as_str().unwrap().to_string();

        if let Some(links_obj) = page.get(subkey) {
            for link in links_obj.as_array().unwrap() {
                links.add(title.clone(), link["title"].as_str().unwrap().to_string());
            }
        }
    }
    links
}

static CLIENT: Lazy<Client> = Lazy::new(Client::new);

fn all_links(prefix: String, prop: String, titles: Vec<String>) -> UnboundedReceiver<Links> {
    let (tx, rx) = tokio::sync::mpsc::unbounded_channel();

    tokio::spawn(async move {
        let mut cont = String::new();
        for batch in titles.chunks(50) {
            let mut first = true;
            while first || !cont.is_empty() {
                let body = get(
                    &CLIENT,
                    build_request(&CLIENT, &prefix, &prop, batch, &cont),
                )
                .await;

                let data: serde_json::Value = serde_json::from_str(&body).unwrap();

                tx.send(extract_links(&data, &prop))
                    .expect("Failed send");
                cont = extract_continue(&data, &format!("{prefix}continue"));

                first = false;
            }
        }
    });

    rx
}

pub fn links_from(titles: Vec<String>) -> UnboundedReceiver<Links> {
    all_links("pl".to_string(), "links".to_string(), titles)
}

pub fn links_here(titles: Vec<String>) -> UnboundedReceiver<Links> {
    all_links("lh".to_string(), "linkshere".to_string(), titles)
}
