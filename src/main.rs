mod api;
mod links;
mod page_graph;

use argh::FromArgs;
use page_graph::PageGraph;
use tokio::time::Instant;

#[derive(FromArgs)]
/// Find path from one wikipedia page to another
struct Args {
    /// page to start on
    #[argh(positional)]
    start: String,

    /// page to end on
    #[argh(positional)]
    end: String,

    /// whether or not to output the time it took to finish the searth
    #[argh(switch)]
    time: bool
}

#[tokio::main]
async fn main() {
    let args: Args = argh::from_env();
    let pg = PageGraph::new();
    let start = args.time.then(Instant::now);

    eprintln!("{}", pg.search(args.start, args.end).await.join(" -> "));

    if let Some(start) = start {
        println!("Finished in {}", start.elapsed().as_secs_f64());
    }
}
