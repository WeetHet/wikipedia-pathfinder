# <center>Wikipedia Pathfinder</center>
## Quickly find paths between wikipedia pages

### Build & install:
Build with `cargo build`, no additional dependencies required

Install with `cargo install`

### Usage:
Run `wikipedia-pathfinder <start> <end>` to find a path between the `<start>` and the `<end>` pages  

If you want to measure the time it took for the program to finish, pass the `--time` flag.